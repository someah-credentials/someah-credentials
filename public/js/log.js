var getCanvas;

$(document).ready(function() { 

	$("#download").on('click', function() { 
		html2canvas(document.querySelector("#card-credentials"), {
			allowTaint: true
		}).then(canvas => {
            // $("#buka").append(canvas);
            getCanvas = canvas;
        });

		const link = document.createElement('a');
        link.download = 'CredentialsCard.png';
	  	link.href = getCanvas.toDataURL("image/png");
	  	link.click();
	}); 

});