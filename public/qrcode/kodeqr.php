<?php 
  include 'qrcode/phpqrcode/qrlib.php';

  function qrku($link)
  {
  	$tempdir = "temp/";
	if (!file_exists($tempdir)) {
		mkdir($tempdir);
	}

	$codeContents = $link;

	QRcode::png($codeContents, $tempdir."qrcode.png");
  }  
?>