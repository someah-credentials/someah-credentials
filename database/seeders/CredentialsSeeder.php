<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CredentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credentials')->insert([
            'username' => 'admin',
            'password' => 'admin12345',
            'link_projek' => 'https://www.admin.com',
            'nama_projek' => 'AD-min',
            'id_user' => '1'
        ]);
    }
}
