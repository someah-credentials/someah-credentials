<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.Logincus');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('users'); 
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/user', [\App\Http\Controllers\userController::class, 'index'])->name('dashboard');

/**
 *	CRUD Data
 */
// Read
Route::middleware(['auth:sanctum', 'verified'])->get('/table', [\App\Http\Controllers\credentialsController::class, 'index'])->name('dashboard');

// Store Create
Route::middleware(['auth:sanctum', 'verified'])->post('/table', [\App\Http\Controllers\credentialsController::class, 'store'])->name('store');

// Create
Route::middleware(['auth:sanctum', 'verified'])->get('/table/create', [\App\Http\Controllers\credentialsController::class, 'create'])->name('create');

// Edit
Route::middleware(['auth:sanctum', 'verified'])->get('/table/{id}/edit', [\App\Http\Controllers\credentialsController::class, 'edit'])->name('edit');

// Update
Route::middleware(['auth:sanctum', 'verified'])->patch('/table/{id}', [\App\Http\Controllers\credentialsController::class, 'update'])->name('update');

// Delete
Route::middleware(['auth:sanctum', 'verified'])->delete('/table/{id}', [\App\Http\Controllers\credentialsController::class, 'destroy'])->name('destroy');



/**
 *  Generate Data
 */
// Preview
Route::middleware(['auth:sanctum', 'verified'])->get('/preview/{id}/preview', [\App\Http\Controllers\credentialsController::class, 'preview'])->name('preview');



/**
 *  Search Data
 */
// Search
Route::middleware(['auth:sanctum', 'verified'])->get('/data/search', [\App\Http\Controllers\credentialsController::class, 'search'])->name('search');