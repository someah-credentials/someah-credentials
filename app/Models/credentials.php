<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class credentials extends Model
{
    use HasFactory;

    protected $fillable = [
    	'username',
    	'password',
    	'link_projek',
    	'nama_projek',
    	'id_user'
    ];

    public function get_IdUser(){
		return $this->hasMany('App\\Models\\User','id_user','id');
	}
}
