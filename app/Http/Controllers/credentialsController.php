<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class credentialsController extends Controller
{
    public function index(){
    	$id = Auth::id();
    	$data['credentials'] = \App\Models\credentials::where('id_user', '=', Auth::user()->id )->get();
        return view('table', $data);
    }

    public function create()
    {
    	return view('form');
    }

    public function store(Request $request)
    {
    	$input = $request->all();

    	$status = \App\Models\credentials::create($input);

    	if ($status) {
    		return redirect('/table');
    	}
    	else {
    		return redirect('/table/create');
    	}
    }

    public function edit(Request $request, $id)
    {
    	$data['credentials'] = \App\Models\credentials::find($id);
    	return view('form', $data);
    }

    public function update(Request $request, $id)
    {
    	$input = $request->all();

    	$data = \App\Models\credentials::find($id);
    	$status = $data->update($input);

    	if ($status) {
    		return redirect('/table');
    	}
    	else {
    		return redirect('/table/{id}/edit');
    	}
    }

    public function destroy(Request $request, $id)
    {
    	$data = \App\Models\credentials::find($id);
    	$status = $data->delete();

    	if ($status) {
    		return redirect('/table')->with('success','Data Berhasil Dihapus');
    	}
    	else {
    		return redirect('/table')->with('error','Data Gagal Dihapus');
    	}
    }

    public function preview(Request $request, $id)
    {
    	$data['credentials'] = \App\Models\credentials::find($id);
    	return view('preview', $data);
    }

    public function search(Request $request)
    {
    	$cari = $request->searchData;

    	if ($cari == '') {
    		$id = Auth::id();
	    	$data['credentials'] = \App\Models\credentials::where('id_user', '=', Auth::user()->id )->paginate();
	        return view('table', $data);
    	}

        $data = \App\Models\credentials::where('username', 'like', '%'.$cari.'%')
                                 		->orWhere('password', 'like', '%'.$cari.'%')
                                 		->orWhere('link_projek', 'like', '%'.$cari.'%')
                                 		->orWhere('nama_projek', 'like', '%'.$cari.'%')
                                 		->paginate();

        return view('table', ['credentials' => $data]);
    }
}
