<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Someah Credentials --Preview</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/preview.css') }}" rel="stylesheet" />

        <script src="{{ asset('js/html2canvas.js') }}"></script>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/log.js') }}"></script>
    </head>
    
    <body id="page-top" class="previews-page">
        <!-- Navigation-->
        <nav class="navbar bg-secondary text-uppercase position-fixed" id="mainNav">
            <div class="container div-atur">
                <div class="navbar-toggler-right" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item content-nav-p1 mx-0 fixed-top mt-1"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href=""> <img src="{{ asset('assets/img/logowebsite.png') }}" alt="logo" wwidth="37" height="37"></a></li>
                   <li class="nav-item mx-0 content-nav-p0 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user') }}">
                         <i class="fas fa-home" id="icon1"></i></a>
                            <p class="text-light text-nav-p1 ml-3">Home</p></li>
                            <li class="nav-item mx-0 content-nav-p2 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/table') }}"><i class="far fa-id-card" id="icon1"></i></a>
                            <p class="text-light text-nav-p1">KARTUKU</p></li>
                        <li class="nav-item content-nav-p3 mx-0 fixed-top ml-2"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user#info') }}"><i class="fas fa-info-circle" id="icon2"></i></a>
                            <p class="text-light text-nav-p2 ml-3">Info</p></li>
                        <li class="nav-item mx-0 content-nav-p4 fixed-bottom ml-1 mb-3">
                         <div class="btn-group dropright">
                         
                            <div class="btn btn-transparent profile dropdown-toggle position-fixed mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="profile-rounded-navbar" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" width="33" height="33">
                            </div>
                            <div class="dropdown-menu" id="popup">
                                <div class="ml-2">{{ Auth::user()->name }}</div>
                                <hr>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                 <span class="text-secondary text-left"> {{ __('Profile') }} </span>
                                </x-jet-dropdown-link>
                                 <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-jet-dropdown-link href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                                       <span class="text-secondary"> {{ __('Logout') }} </span>
                                    </x-jet-dropdown-link>
                                </form>
                            </div>
                            </div>
                        
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- About Section-->
        <section class="page-section text-white mb-0" id="preview2">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="judul-preview page-section-heading text-center text-uppercase text-white">preview</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- About Section Content-->
                <div class="container">
                    <center>
                        <p class="head-preview">Pastikan data sudah benar! Jika sudah silahkan download.</p>
                        <div id="card-credentials" class="output-card">
                            <header class="container" >
                            <div>
                                <img id="logo" src="{{ asset('assets/img/someah-logo.png') }}" alt="logo">  
                            </div>
                            </header>
                            <section class="container">
                                <div>
                                    <div id="project" class="container pt-2">
                                        <h4>{{ @$credentials->nama_projek }}</h4>
                                    </div>   
                                <div>
                                    <div class="row">
                                        <div class="nama pt-2">
                                            <p>Username &nbsp;: <span>{{ @$credentials->username }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="row">
                                        <div class="pass pt-2">
                                            <p>Password &nbsp; : <span>{{ @$credentials->password }}</span></p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div>
                                    <div class="row">
                                        <div class="">
                                            <?php 
                                              include 'qrcode/kodeqr.php';

                                              qrku(@$credentials->link_projek);
                                            ?>
                                            <img class="scan" src="{{ asset('temp/qrcode.png') }}">
                                        </div>
                                        <div class="link">
                                            <p>{{ @$credentials->link_projek }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </section>
                            <footer class=" foot container">
                                <div class="row">
                                    <div id="whatssap">
                                        <img id="picwa" src="{{ asset('assets/img/whatsapp_48px.png') }}" alt="whatssap">
                                        <a>+628562294222</a>
                                    </div>
                                    <div id="gmail">
                                        <img id="picgmail" src=" {{ asset('assets/img/gmail_48px.png') }}" alt="gmail">
                                        <a>info@someah.id</a> 
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </center>
                </div>
                <!-- About Section Button-->
                <div class="text-center mt-4">
                    <button id="download" type="submit" class="btn btn-md btn-outline-light">
                        <i class="fas fa-download mr-2"></i>
                        Download
                    </button>
                </div>
            </div>
        </section>
        
        <!-- Bootstrap core JS-->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    </body>
</html>
