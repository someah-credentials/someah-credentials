<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Someah Credentials --Table</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('assets/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- <link rel="stylesheet" href="css/font-awesome.min.css"> -->
  
   <!-- Font Awesome Icons -->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
     integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- Custom styles for this template-->
  <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/tablepage.css') }}" rel="stylesheet" />

</head>

<body id="page-top">
<nav class="navbar bg-secondary text-uppercase position-fixed" id="mainNav">
            <div class="container div-atur">
                <div class="navbar-toggler-right" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item content-nav-p1 mx-0 fixed-top mt-1"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="#"> <img src="{{ asset('assets/img/logowebsite.png') }}" alt="logo" wwidth="37" height="37"></a></li>
                        <li class="nav-item mx-0 content-nav-p0 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user') }}">
                         <i class="fas fa-home" id="icon1"></i></a>
                            <p class="text-light text-nav-p1 ml-3">Home</p></li>
                            <li class="nav-item mx-0 content-nav-p2 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/table') }}"><i class="far fa-id-card" id="icon1"></i></a>
                            <p class="text-light text-nav-p1">KARTUKU</p></li>
                        <li class="nav-item content-nav-p3 mx-0 fixed-top ml-2"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user#info') }}"><i class="fas fa-info-circle" id="icon2"></i></a>
                            <p class="text-light text-nav-p2 ml-3">Info</p></li>
                        <li class="nav-item mx-0 content-nav-p4 fixed-bottom ml-1 mb-3">
                         <div class="btn-group dropright">
                         
                            <div class="btn btn-transparent profile dropdown-toggle position-fixed mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="profile-rounded-navbar" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" width="33" height="33">
                            </div>
                            <div class="dropdown-menu" id="popup">
                                <div class="ml-2">{{ Auth::user()->name }}</div>
                                <hr>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                 <span class="text-secondary text-left"> {{ __('Profile') }} </span>
                                </x-jet-dropdown-link>
                                 <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-jet-dropdown-link href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                                       <span class="text-secondary"> {{ __('Logout') }} </span>
                                    </x-jet-dropdown-link>
                                </form>
                            </div>
                            </div>
                        
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <form action="{{ url('/data/search') }}" method="GET">
          <div class="input-group md-form form-sm form-2 pl-0 search-form">
            <input class="form-control my-0 py-1 mt-4" type="text" name="searchData" value="{{ old('searchData') }}" placeholder="Search" aria-label="Search">
            <div class="input-group-append mt-4">
              <button type="submit" class="input-group-text btn btn-primary" id="basic-text1"> 
                <i class="fas fa-search text-grey" aria-hidden="true"></i>
              </button>
            </div>
          </div>
        </form>
          
 <!-- Begin Page Content -->
        <!-- <div class="container-fluid bg-warning"> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4 mt-5 kartu">
            <div class="card-header py-3 row ml-1 mr-1">
              <div class="col-md-5 mr-5">
                <h4 class="m-0 font-weight-bold text-primary">Data Credentials</h4>
              </div>
              <div class="col-md-4"><p></p></div>
              <div class="col-md-2 ml-5 text-right">
                <a href="{{ url('/table/create') }}" class="btn btn-info">
                  Tambah Data
                </a>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Link</th>
                      <th>Nama_project</th>
                      <th>Aksi</th>
                      <th> </th>
                    </tr>
                  </thead>

                  @foreach ($credentials as $row)
                  <tbody>
                    <tr>
                      <td> {{ isset($i) ? ++$i : $i = 1 }}. </td>
                      <td> {{ $row->username }} </td>
                      <td> {{ $row->password }} </td>
                      <td> {{ $row->link_projek }} </td>
                      <td> {{ $row->nama_projek }} </td>
                      <td>
                        <div class="row">
                          <div class="col-md-1">
                            <form action="{{ url('/table/' . $row->id . '/edit') }}">
                              <button class="btn btn-warning"><i class="far fa-edit"></i></button>
                            </form>
                          </div>
                          <div class="col-md-1">
                            <form action="{{ url('/table/'. $row->id) }}" method="POST">
                              @method('DELETE')
                              @csrf

                              <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                            </form>
                          </div>
                        </div>
                      </td>
                      <td>
                        <form action="{{ url('/preview/' . $row->id . '/preview') }}">
                          <button class="btn btn-primary"><i class="fas fa-print"></i></button>
                        </form>
                      </td>
                    </tr>
                  </tbody>
                  @endforeach

                  <tfoot>
                    <tr>
                      <th>No.</th>
                      <th>Username</th>
                      <th>Password</th>
                      <th>Link</th>
                      <th>Nama_project</th>
                      <th>Aksi</th>
                      <th> </th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

 <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>


</body>

</html>