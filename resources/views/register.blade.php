<!doctype html>
<html lang="zxx">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/register.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <title>Form Register</title>
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">
            <img class="logo" src="assets/images/someah-logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        </div>
      </nav>
      
      <section class="container">   
        <div class="border shadow">
          <div class="hallo">
            <div class="row">
              <div class="col-lg-5 my-3">
                <a href="Login.html" class="signin">Sign In</a>
              </div>
              <div class="col-lg-5 ml-5 my-3">
              <a href="register.html" class="signup">Sign Up</a>
            </div>
            </div>
          </div>
            <div class="container pt-4">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-2" src="assets/images/name_30px.png" alt="">
              </div>
              <div>
                <input class="mail form-control" type="text" placeholder="Email">
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-3" src="assets/images/envelope_26px.png" alt="">
              </div>
              <div>
                <input class="user form-control" type="text" placeholder="Username">
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-3" src="assets/images/lock_30px.png" alt="">
              </div>
              <div>
                <input class="pass form-control" type="text" placeholder="Password">
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
              <div>
                <input class="conpass form-control" type="text" placeholder="Confirm Password">
              </div>
              </div>
            </div>
            <div class="container">
                <button id="btn">Sign Up</button>
            </div>
            <div class="link">
                <a href="">Sign In with Gmail</a>
            </div>
            <div class="image position-fixed">
              <img src="assets/images/imghead.png" alt="">
            </div>
        </div>
      </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
  </body>
</html>