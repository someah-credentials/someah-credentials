<?php
    use Illuminate\Support\Facades\Auth;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Someah Credentials</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/preview.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
    </head>
    
    <body>
        <!-- Navigation-->
        <nav class="navbar bg-secondary text-uppercase position-fixed" id="mainNav">
            <div class="container div-atur">
                <div class="navbar-toggler-right" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item content-nav-p1 mx-0 fixed-top mt-1"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="#"> <img src="{{ asset('assets/img/logowebsite.png') }}" alt="logo" wwidth="37" height="37"></a></li>
                        <li class="nav-item mx-0 content-nav-p0 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user') }}">
                         <i class="fas fa-home" id="icon1"></i></a>
                            <p class="text-light text-nav-p1 ml-3">Home</p></li>
                            <li class="nav-item mx-0 content-nav-p2 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/table') }}"><i class="far fa-id-card" id="icon1"></i></a>
                            <p class="text-light text-nav-p1">KARTUKU</p></li>
                        <li class="nav-item content-nav-p3 mx-0 fixed-top ml-2"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user#info') }}"><i class="fas fa-info-circle" id="icon2"></i></a>
                            <p class="text-light text-nav-p2 ml-3">Info</p></li>
                        <li class="nav-item mx-0 content-nav-p4 fixed-bottom ml-1 mb-3">
                         <div class="btn-group dropright">
                         
                            <div class="btn btn-transparent profile dropdown-toggle position-fixed mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="profile-rounded-navbar" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" width="33" height="33">
                            </div>
                            <div class="dropdown-menu" id="popup">
                                <div class="ml-2">{{ Auth::user()->name }}</div>
                                <hr>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                 <span class="text-secondary text-left"> {{ __('Profile') }} </span>
                                </x-jet-dropdown-link>
                                 <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-jet-dropdown-link href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                                       <span class="text-secondary"> {{ __('Logout') }} </span>
                                    </x-jet-dropdown-link>
                                </form>
                            </div>
                            </div>
                        
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Contact Section-->
        <section class="page-section" id="input">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">input kredensial
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                        <form action="{{ url('/table', @$credentials->id) }}" method="POST" id="contactForm" name="sentMessage" novalidate="novalidate">
                            @csrf

                            @if(!empty($credentials))
								@method('PATCH')
							@endif

                            <div class="control-group">
                                <div class="text-left form-group controls mb-0 pb-2">
                                    <h3><label>Nama :</label><h3>
                                    <input class="form-control" name="username" id="username" type="text" value="{{ old('username', @$credentials->username) }}" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="text-left mt-5 form-group controls mb-0 pb-2">
                                    <h3><label>Password :</label><h3>
                                    <input class="form-control" name="password" id="password" type="text" value="{{ old('password', @$credentials->password) }}" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="text-left mt-5 form-group controls mb-0 pb-2">
                                    <h3><label>Link :</label><h3>
                                    <input class="form-control" name="link_projek" id="link" type="text" value="{{ old('link_projek', @$credentials->link_projek) }}" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="text-left mt-5 form-group controls mb-0 pb-2">
                                    <h3><label>Nama Project :</label><h3>
                                        <input class="form-control" name="nama_projek" id="nama project" type="text" value="{{ old('nama_projek', @$credentials->nama_projek) }}" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <input type="text" name="id_user" hidden="" value="{{ Auth::user()->id }}">
                            <br />
                            <button type="submit" class="btn btn-outline-secondary btn-md" id="sendMessageButton">
                                @if(empty($credentials))
                                    Tambah Data
                                @endif

                                @if(!empty($credentials))
                                    Edit Data
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        
        <!-- Bootstrap core JS-->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    </body>
</html>
