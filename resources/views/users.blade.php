<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Someah Credentials</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/preview.css') }}" rel="stylesheet" />
    </head>
    
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar bg-secondary text-uppercase position-fixed" id="mainNav">
            <div class="container div-atur">
                <div class="navbar-toggler-right" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item content-nav-p1 mx-0 fixed-top mt-1"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href=""> <img src="{{ asset('assets/img/logowebsite.png') }}" alt="logo" wwidth="37" height="37"></a></li>
                        <li class="nav-item mx-0 content-nav-p0 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/user') }}"><i class="fas fa-home" id="icon1"></i></a>
                            <p class="text-light text-nav-p1 ml-3">Home</p></li>
                            <li class="nav-item mx-0 content-nav-p2 fixed-top ml-2" style="width: 55px;"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ url('/table') }}"><i class="far fa-id-card" id="icon2"></i></a>
                            <p class="text-light text-nav-p1">KARTUKU</p></li>
                        <li class="nav-item content-nav-p3 mx-0 fixed-top ml-2"><a class="nav-link py-1 px-0 px-lg-3 rounded js-scroll-trigger" href="#info"><i class="fas fa-info-circle" id="icon2"></i></a>
                            <p class="text-light text-nav-p2 ml-3">Info</p></li>
                        <li class="nav-item mx-0 content-nav-p4 fixed-bottom ml-1 mb-3">
                         <div class="btn-group dropright">
                            <div class="btn btn-transparent profile dropdown-toggle position-fixed mb-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="profile-rounded-navbar" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" width="33" height="33">
                            </div>
                            <div class="dropdown-menu" id="popup">
                                <div class="ml-2">{{ Auth::user()->name }}</div>
                                <hr>
                                <x-jet-dropdown-link href="{{ route('profile.show') }}">
                                 <span class="text-secondary text-left"> {{ __('Profile') }} </span>
                                </x-jet-dropdown-link>
                                 <!-- Authentication -->
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf

                                    <x-jet-dropdown-link href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    this.closest('form').submit();">
                                       <span class="text-secondary"> {{ __('Logout') }} </span>
                                    </x-jet-dropdown-link>
                                </form>
                            </div>
                            </div>
                        
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead text-white text-center bg-custom">
            <div class="container d-flex align-items-center flex-column">
                <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-5" src="assets/img/avataaars.svg" alt="avatar" />
                <!-- Masthead Heading-->
                <h1 class="masthead-heading text-white text-uppercase mb-0">someah credentials</h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
            </div>
        </header>
        <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-arrow-up"></i></button>
        <section class="page-section text-white mb-0">
            <h3 class="masthead-heading text-secondary text-uppercase mb-0 text-center"><label> Panduan penggunaan aplikasi!</label></h3>
            <div class="divider-custom divider-secondary mb-5">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <p class="masthead-subheading pb-5 font-weight-bold h5 text-secondary text-center mb-0">Yoo! Buat kartu kredensialmu sekarang. <br> Disini kamu bisa menginputkan data dan mengeksport-nya ke dalam bentuk image.</p>
            <div class="container" id="panduan">
              <div class="row">
              <div class="col-lg-3 mb-5 ml-5 bg-white rounded shadow">
                        <div class="portfolio-item">
                            <div class="portfolio-item-caption ml-5 py-3">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/43-removebg-preview.png') }}" width="100" height="60" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 1</h6>
                               <hr>
                               <p class="text-secondary text-center">Input data pada form kredensial.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-5 bg-white ml-5 rounded shadow">
                        <div class="portfolio-item">
                            <div class="portfolio-item-caption ml-5 py-3">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/5607-removebg-preview.png') }}" width="100" height="60" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 2</h6>
                               <hr>
                               <p class="text-secondary text-center">Simpan data ke database</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-5 ml-5 bg-white rounded shadow">
                        <div class="portfolio-item py-3">
                            <div class="portfolio-item-caption ml-5">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/32935-removebg-preview.png') }}" width="110" height="85" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 3</h6>
                               <hr>
                               <p class="text-secondary text-center">Lihat table database</p>
                            </div>
                        </div>
                    </div>
              </div>
              <div class="row">
                    <div class="col-lg-3 mb-5 bg-white ml-5 rounded shadow">
                        <div class="portfolio-item">
                            <div class="portfolio-item-caption ml-5 py-3">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/3891942-removebg-preview.png') }}" width="100" height="60" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 4</h6>
                               <hr>
                               <p class="text-secondary text-center">Tekan tombol generate di table database</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-5 bg-white ml-5 rounded shadow">
                        <div class="portfolio-item">
                            <div class="portfolio-item-caption ml-5 py-3">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/2164248-removebg-preview.png') }}" width="100" height="60" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 5</h6>
                               <hr>
                               <p class="text-secondary text-center">Cek data di preview</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-5 bg-white ml-5 rounded shadow">
                        <div class="portfolio-item">
                            <div class="portfolio-item-caption ml-5 py-3">
                                <img class="img-fluid ml-4" src="{{ asset('assets/img/Passing-by-removebg-preview.png') }}" width="100" height="60" alt="" />
                            </div>
                            <div class="portfolio-item-caption">
                               <h6 class="masthead-heading text-secondary text-uppercase mb-0 text-center">STEP 6</h6>
                               <hr>
                               <p class="text-secondary text-center">Selesai generate!</p>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </section>
        <!-- About Section-->
        <section class="page-section text-white mb-0" id="preview">
            <div class="container">
                <!-- About Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-white">Buat kartu kredensial kamu sekarang!</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- About Section Button-->
                <div class="text-center mt-4">
                    <a type="submit" class="text-light btn btn-md btn-outline-light" href="{{ url('/table/create') }}">
                        Buat sekarang
                    </a>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer text-center" id="info">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Alamat kantor</h4>
                        <p class="lead mb-0">
                        Office: <a class="text-white" href="https://www.google.com/maps/place/Jl.+Cisitu+Indah+4+Jl.+Cisitu+Indah+III+No.2,+Dago,+Kecamatan+Coblong,+Kota+Bandung,+Jawa+Barat+40135/@-6.8761343,107.6101589,17z/data=!3m1!4b1!4m5!3m4!1s0x2e68e6e528dbc027:0x516181cabf7baae3!8m2!3d-6.8761396!4d107.6123476">
                        Jl. Cisitu Indah III No. 2, Dago, Coblong, Kota Bandung, Jawa Barat, 40135</a></p>
                        <p class="lead mb-0">
                        Workshop:  <a class="text-white" href="https://www.google.com/maps/place/Someah+Kreatif+Nusantara/@-6.8824894,107.6148598,15z/data=!4m5!3m4!1s0x0:0x737853f7eedf64a9!8m2!3d-6.8824894!4d107.6148598"> 
                        Jl. Ir. H. Juanda No.203B, Dago, Coblong, Kota Bandung, Jawa Barat 40135
                        </a></p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Kontak dan sosial media</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="mailto:info@someah.id"><i class="fa fa-envelope"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.instagram.com/someahkreatif/"><i class="fab fa-fw fa-instagram"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.linkedin.com/company/somearch-nusantara/"><i class="fab fa-fw fa-linkedin-in"></i></a>
                        <!-- <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a> -->
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">Website Resmi Kami</h4>
                        <p class="lead mb-0">
                            PT. Someah Kreatif Nusantara
                        </p>
                        <p class="lead mb-0 mt-5">
                        <a href="https://someah.id/" class="btn btn-outline-light">Kunjungi</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container">
                <small>Copyright © Your Website 2020</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Third party plugin JS-->
        <script src="{{ asset('assets/js/jquery.easing.min.js') }}"></script>
        <!-- Contact form JS-->
        <script src="{{ asset('assets/mail/jqBootstrapValidation.js') }}"></script>
        <script src="{{ asset('assets/mail/contact_me.js') }}"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('js/scripts.js') }}"></script>
        <script src="{{ asset('assets/js/up.js') }}"></script>
    </body>
</html>
