<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Forgot your password</title>
     <!-- Font Awesome icons (free version)-->
     <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
   <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
   <link rel="stylesheet" href="{{ asset('assets/css/forgot.css') }}">
    </head>
  <body background="assets/img/bglogin.png">
  <x-guest-layout>
  @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600 position-fixed" style="margin-top: 150px; margin-left: 80px;">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="position-fixed" style="margin-top: 130px; margin-left: 80px;" />
        <img src="{{ asset('assets/img/someah-logo.png') }}" class="ml-4 mt-4 " width="160" height="56.72" alt="Someah-logo">
            <img src="{{ asset('assets/img/3293465-removebg-preview 1.png') }}" class="position-fixed img-ilustrasi" width="600" height="600" alt="Ilustrasi">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="p-3 bg-white rounded shadow blok-div ">
              <img src="{{ asset('assets/img/help_50px 1.png') }}" class="center-block" width="50px" height="50px" alt="help">
              <p class="text-center mt-2">Lupa Password?</p>
              <p class="text-center">kami membutuhkan email terdaftarmu untuk <br> memverifikasi reset password</p>
              <div class="row shadow-sm ml-5 form-forgot bg-white">
                <!-- <div class="col-sm-2" style="border: 1px solid black;"> -->
                    <img src="{{ asset('assets/img/post_50px 2.png') }}" class="mt-1 ml-2" alt="Forgot-email">
                  <!-- </div> -->
                <div>
                <input id="email" class="block ml-3 mt-1 w-full form-control mt-1" type="email" name="email" :value="old('email')" required autofocus />
                </div>
              </div>
              <div class="row mt-4">
              <div class="col-sm-4">
                <a class="text-secondary ml-3" href="{{ route('login') }}"><i class="fas fa-arrow-left"></i>&nbsp; Kembali</a>
              </div>
              <div class="col-sm-4">
                <button class="custom text-white">
                    {{ __('Send email') }}
                </button>
            </div>
            </div>
            </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
          </form>
    </x-guest-layout>
  </body>
</html>
