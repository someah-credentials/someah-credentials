<!doctype html>
<html lang="zxx">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/register.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <title>Form Register</title>
  </head>
  <body>
        <div class="container fixed-top">
            <a class="navbar-brand" href="#">
            <img class="logo" src="{{ asset('assets/img/someah-logo.png') }}" alt="Logo-someah">
        </a>
        </div>
        <x-guest-layout>
        <x-jet-validation-errors class="container position-fixed fixed-top" style="margin-top: 130px; margin-left: 460px;"/>

<form method="POST" action="{{ route('register') }}">
    @csrf
      
      <section class="container">   
        <div class="border shadow">
          <div class="hallo">
            <div class="row">
              <div class="col-lg-5 my-3">
                <a href="{{ route('login') }}" class="signin">Sign In</a>
              </div>
              <div class="col-lg-5 ml-5 my-3">
              <a href="#" class="signup">Sign Up</a>
            </div>
            </div>
          </div>
            <div class="container pt-4">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-2" src="{{ asset('assets/img/name_30px.png') }}" alt="">
              </div>
              <div>
              <x-jet-input id="name" class="block mt-1 w-full mail form-control" type="text" name="name" :value="old('name')" placeholder="name" required autofocus autocomplete="name" />
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-3" src="{{ asset('assets/img/envelope_26px.png') }}" alt="envelope">
              </div>
              <div>
              <x-jet-input id="email" class="block mt-3 w-full user form-control" placeholder="Email" type="email" name="email" :value="old('email')" required />
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-3" src="{{ asset('assets/img/lock_30px.png') }}" alt="Lock">
              </div>
              <div>
              <x-jet-input id="password" placeholder="Password" class="pass block mt-3 w-full form-control" type="password" name="password" required autocomplete="new-password" />
              </div>
              </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                <img class="size-image mt-2 ml-2 pt-3" src="{{ asset('assets/img/lock_30px.png') }}" alt="Lock">
              </div>
              <div>
              <x-jet-input id="password_confirmation" placeholder="Confirm Password" class="pass block mt-3 w-full form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
              </div>
              </div>
            </div>
            <div class="items-center mt-4 ml-5">
                <button id="btn">
                    {{ __('Register') }}
                </button>
            </div>
            <div class="image position-fixed">
              <img src="{{ asset('assets/img/imghead.png') }}" alt="imghead">
            </div>
        </div>
      </section>
        </form>
    </x-guest-layout>
  </body>
</html>