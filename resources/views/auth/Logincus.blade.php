<!doctype html>
<html lang="zxx">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    
    <title>Form Login</title>
  </head>
  <body>
  <x-guest-layout>

  <a class="navbar-brand ml-5 mt-2 fixed-top" href="#">
            <img class="logo" src="{{ asset('assets/img/someah-logo.png') }}" alt="Logo-login">
        </a>
        <x-jet-validation-errors class="container position-fixed fixed-top" style="margin-top: 117px; margin-left: 160px;"/>
        @if (session('status'))
          <div class="mb-4 font-medium text-sm text-green-600">
              {{ session('status') }}
          </div>
      @endif
      <section class="container"> 
      <form method="POST" style="margin-left: 6%;" action="{{ route('login') }}">
            @csrf
        <div class="border shadow">
            <div class="hallo">
              <div class="row">
                <div class="col-lg-5 my-3">
                  <a href="Login.html" class="signin">Sign In</a>
                </div>
                <div class="col-lg-5 ml-5 my-3">
                  @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-3 signup underline">Sign Up</a>
                  @endif
              </div>
              </div>
            </div>
            <div class="row my-2">
                <div class="col-sm-2">
                <img class="icon1 size-image mt-2 ml-2" src="{{ asset('assets/img/name_30px.png') }}" alt="Icon-login">
                </div>
                <div>
                <div>
                <!-- <x-jet-label for="email" value="{{ __('Email') }}" /> -->
                <input id="email" class="in1 form-control block mt-1 w-full" placeholder="Email" type="email" name="email" :value="old('email')" required autofocus />
            </div>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-2">
              <img class="icon2 size-image mt-4 ml-2" src="{{ asset('assets/img/lock_30px.png') }}" alt="Icon-login">
              </div>
              <div col-md-5>
                  <div class="mt-4">
                    <!-- <x-jet-label for="password" value="{{ __('Password') }}" /> -->
                    <input id="password" class=" in2 form-control block mt-1 w-full" placeholder="Password" type="password" name="password" required autocomplete="current-password" />
                </div>
              </div>
            </div>

            <div style="margin-left: 10%;">
                <button class="ml-4" id="btn">
                    {{ __('Login') }}
                </button>
            </div>
          <div class="row my-3 ml-5">
          <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm pl-5 text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
          </div>
        </div>
            <div class="image position-fixed" >
              <img src="{{ asset('assets/img/3327590-removebg-preview.png') }}" alt="Image-login">
            </div>
        </div>
        </form>
      </section>
      </x-guest-layout>
  </body>
</html>